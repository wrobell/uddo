import cytoolz.functoolz as ftz
import io
import operator as op
import os.path
import owlrl
import rdflib

DATA_DIVE = """
@prefix : <#>.
@prefix foaf: <http://xmlns.com/foaf/0.1/>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
@prefix uddo: <http://www.uddf.org/ontology/uddo/1.0/>.

:diver a uddo:Diver.

:d1 uddo:diver :diver;
    uddo:diveNumber 1;
    uddo:diveDate "2022-01-01"^^xsd:date;
    uddo:depth "30.0"^^xsd:float;
    uddo:duration 3200.
:d2 uddo:diver :diver;
    uddo:diveNumber 2;
    uddo:diveDate "2022-01-01"^^xsd:date;
    uddo:diveTime "10:04:05"^^xsd:time;
    uddo:depth "30.0"^^xsd:float;
    uddo:duration 3200.

#
# not a dive
#

# missing diver
:d3 uddo:diveNumber 3;
    uddo:diveDate "2022-01-01"^^xsd:date;
    uddo:depth "30.0"^^xsd:float;
    uddo:duration 3200.

# missing diveDate
:d4 uddo:diver :diver;
    uddo:diveNumber 3;
    uddo:depth "30.0"^^xsd:float;
    uddo:duration 3200.

# missing diveNumber
:d5 uddo:diver :diver;
    uddo:diveDate "2022-01-01"^^xsd:date;
    uddo:depth "30.0"^^xsd:float;
    uddo:duration 3200.

# invalid diveDate data type
:d6 uddo:diveDate 1;
    uddo:diveNumber 1;
    uddo:depth "30.0"^^xsd:float;
    uddo:duration 3200.

# invalid diveNumber data type
:d7 uddo:diveDate "2022-01-01"^^xsd:date;
    uddo:diveNumber "2022-01-01"^^xsd:date.

# wrong cardinality
##   :d8 uddo:diveDate "2022-01-01"^^xsd:date;
##       uddo:diveDate "2022-01-02"^^xsd:date;
##       uddo:diveNumber 1;
##       uddo:diveNumber 2.
"""

def test_dive_inference() -> None:
    graph = rdflib.Graph()
    with open('ontology/uddo.n3') as f:
        graph.parse(f, format='n3')
    graph.parse(io.StringIO(DATA_DIVE), format='n3')
    owlrl.DeductiveClosure(owlrl.OWLRL_Semantics).expand(graph)

    result = graph.query("""
    PREFIX uddo: <http://www.uddf.org/ontology/uddo/1.0/>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

    SELECT ?d
    WHERE {
        ?d a uddo:Dive.
    }
    ORDER BY ?d
    """)

    to_id = ftz.compose_left(op.itemgetter(0), str, os.path.basename)
    dives = [to_id(v) for v in result]
    assert dives == ['#d1', '#d2']

# vim: sw=4:et:ai
