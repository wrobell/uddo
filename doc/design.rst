Dive
====
Dive duration is a number of seconds, XSD float data type. Use of XSD
duration data type is not recommended :cite:p:`w3c:xsd-datatypes`.

.. vim: sw=4:et:ai
